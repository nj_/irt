@echo off

set basedir=%cd%
set code=%basedir%\src

set opts=-FC -GR- -EHa -nologo -W4 -nologo
set linker_opts=-incremental:no

set dll_opts=-D_USRDLL -D_WINDLL
set dll_linker_opts=%linker_opts% kernel32.lib User32.lib -dll

REM Enter here the full path for visual studio setup script.
REM This will allow us to call to cl.exe without having to deal with the madness of Visual Studio.
call "%VC_PATH%\VC\Auxiliary\Build\vcvarsall.bat" x64

mkdir build         2>nul
mkdir build\Debug   2>nul
mkdir build\Release 2>nul

mkdir ShadowModelerGH       2>nul
mkdir ShadowModelerGH\Build 2>nul

pushd build\Debug
cl.exe %opts% -Zi %dll_opts% -DSHMD_DEBUG %code%\shadow_modeler.c -link %dll_linker_opts% -out:shadow_modeler.dll
cl.exe %opts% -Zi %code%\main.c shadow_modeler.exp shadow_modeler.lib -FeShadowModeler.exe -link %linker_opts%
mkdir %basedir%\ShadowModelerGH\Build\Debug 2>nul
copy shadow_modeler.dll %basedir%\ShadowModelerGH\Build\Debug
copy shadow_modeler.pdb %basedir%\ShadowModelerGH\Build\Debug
popd

pushd build\Release
cl.exe %opts% -O2 %dll_opts%  %code%\shadow_modeler.c -link %dll_linker_opts% -out:shadow_modeler.dll
cl.exe %opts% -O2 %code%\main.c shadow_modeler.exp shadow_modeler.lib -FeShadowModeler.exe -link %linker_opts%
mkdir %basedir%\ShadowModelerGH\Build\Release 2>nul
copy shadow_modeler.dll %basedir%\ShadowModelerGH\Build\Release
popd