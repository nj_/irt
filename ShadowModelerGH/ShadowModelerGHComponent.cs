﻿using Grasshopper.Kernel;
using Rhino.Geometry;
using System;
using System.IO;
using System.Text;
using System.Collections.Generic;
using System.Runtime.InteropServices;


// In order to load the result of this wizard, you will also need to
// add the output bin/ folder of this project to the list of loaded
// folder in Grasshopper.
// You can use the _GrasshopperDeveloperSettings Rhino command for that.

namespace ShadowModelerGH
{
    public class ShadowModelerGHComponent : GH_Component
    {
        IntPtr shadow_model_state = (IntPtr)null;
        /// <summary>
        /// Each implementation of GH_Component must provide a public 
        /// constructor without any arguments.
        /// Category represents the Tab in which the component will appear, 
        /// Subcategory the panel. If you use non-existing tab or panel names, 
        /// new tabs/panels will automatically be created.
        /// </summary>
        public ShadowModelerGHComponent()
          : base("ShadowModelerGH", "ShadowModeler",
              "Creates a model out of a set of given shadows (black and white images).",
              "CSArch", "Generator")
        {
        }

        /// <summary>
        /// Registers all the input parameters for this component.
        /// </summary>
        protected override void RegisterInputParams(GH_Component.GH_InputParamManager pManager)
        {
            // Use the pManager object to register your input parameters.
            // You can often supply default values when creating parameters.
            // All parameters must have the correct access type. If you want 
            // to import lists or trees of values, modify the ParamAccess flag.
            pManager.AddTextParameter("Image Directory", "Path", "The directory in which the images for the transform are stored.", GH_ParamAccess.item, "");
            pManager.AddIntegerParameter("Voxel Grid Resolution", "Res", "The resolution for the voxel grid that will be used to generate the model.", GH_ParamAccess.item, 32);
            pManager.AddIntegerParameter("Images Per Rotation", "IPR", "The amount of images that will be projected in a single 180-degrees rotation.", GH_ParamAccess.item, 3);
            // If you want to change properties of certain parameters, 
            // you can use the pManager instance to access them by index:
            //pManager[0].Optional = true;
        }

        /// <summary>
        /// Registers all the output parameters for this component.
        /// </summary>
        protected override void RegisterOutputParams(GH_Component.GH_OutputParamManager pManager)
        {
            // Use the pManager object to register your output parameters.
            // Output parameters do not have default values, but they too must have the correct access type.
            pManager.AddPointParameter("Points", "Points", "A volumetric pointcloud.", GH_ParamAccess.list);
            pManager.AddMeshParameter("Mesh", "Mesh", "A triangular mesh.", GH_ParamAccess.list);

            // Sometimes you want to hide a specific parameter from the Rhino preview.
            // You can use the HideParameter() method as a quick way:
            //pManager.HideParameter(0);
        }

        /// <summary>
        /// This is the method that actually does the work.
        /// </summary>
        /// <param name="DA">The DA object can be used to retrieve data from input parameters and 
        /// to store data in output parameters.</param>
        unsafe protected override void SolveInstance(IGH_DataAccess DA)
        {
            string image_library_path = "images";
            if (!DA.GetData(0, ref image_library_path)) return;
            int grid_resolution = 128;
            if (!DA.GetData(1, ref grid_resolution)) return;
            int images_per_rotation = 3;
            if (!DA.GetData(2, ref images_per_rotation)) return;


            if (grid_resolution <= 0) {
                AddRuntimeMessage(GH_RuntimeMessageLevel.Error, "Grid resolution cannot be zero or negative!");
                return;
            }

            if (images_per_rotation <= 0) {
                AddRuntimeMessage(GH_RuntimeMessageLevel.Error, "Images per rotation cannot be zero or negative!");
                return;
            }

            if (!Directory.Exists(image_library_path)) {
                AddRuntimeMessage(GH_RuntimeMessageLevel.Error, string.Format("Could not find '{0}'!", image_library_path));
                return;
            }

            string[] image_files = Directory.GetFiles(image_library_path, "*.png", SearchOption.TopDirectoryOnly);
            
            if (image_files.Length == 0) {
                AddRuntimeMessage(GH_RuntimeMessageLevel.Error, string.Format("Could not find png images in '{0}'!", image_library_path));
                return;
            }

            shadow_model_state = ShadowModelerUser.shmd_init_state(shadow_model_state, grid_resolution, images_per_rotation);               

            foreach(string image_filename in image_files) {
                byte[] c_input_string = Encoding.ASCII.GetBytes(image_filename);
                ShadowModelerUser.shmd_add_projection(shadow_model_state, c_input_string, 360.0f, 360.0f);
            }

            ShadowModelerUser.shmd_generate_volumetric_data(shadow_model_state);

            {
                ShadowModelerUser.shmd_maybe_allocate_vertex_buffer(shadow_model_state);
                int vertex_count = ShadowModelerUser.shmd_generate_pointcloud(shadow_model_state);
                if (vertex_count == 0) {
                    AddRuntimeMessage(GH_RuntimeMessageLevel.Error, "0 points have been generated!");
                    return;
                }

                float* vertex_buffer;

                unsafe {
                    vertex_buffer = ShadowModelerUser.shmd_get_vertecies(shadow_model_state);
                }
                if (vertex_buffer == null) {
                    AddRuntimeMessage(GH_RuntimeMessageLevel.Error, "Failed to get vertex buffer!");
                    return;
                }

                List<Point3d> pointcloud = new List<Point3d>();

                for (int i = 0; i < vertex_count; ++i) {
                    float x = vertex_buffer[i * 3 + 0];
                    float y = vertex_buffer[i * 3 + 1];
                    float z = vertex_buffer[i * 3 + 2];
                    pointcloud.Add(new Point3d(x, y, z));
                }

                DA.SetDataList(0, pointcloud);
            }

            {
                int triangle_count = ShadowModelerUser.shmd_march_cubes(shadow_model_state);
                if (triangle_count == 0) {
                    AddRuntimeMessage(GH_RuntimeMessageLevel.Error, "0 triangles have been generated!");
                    return;
                }

                float* triangle_buffer;
                unsafe {
                    triangle_buffer = ShadowModelerUser.shmd_get_vertecies(shadow_model_state);
                }
                if (triangle_buffer == null) {
                    AddRuntimeMessage(GH_RuntimeMessageLevel.Error, "Failed to get vertex buffer!");
                    return;
                }

                Mesh mesh = new Mesh();

                for (int i = 0; i < triangle_count; ++i) {
                    int t_i = i * 9;
                    for (int j = 0; j < 3; ++j) {
                        int p_i = j * 3;
                        float x = triangle_buffer[t_i + p_i + 0];
                        float y = triangle_buffer[t_i + p_i + 1];
                        float z = triangle_buffer[t_i + p_i + 2];
                        mesh.Vertices.Add(x, y, z);
                    }
                    mesh.Faces.AddFace(i * 3 + 0, i * 3 + 1, i * 3 + 2);
                }

                mesh.Vertices.CombineIdentical(true, true);

                DA.SetData(1, mesh);
            }
        }

        /// <summary>
        /// The Exposure property controls where in the panel a component icon 
        /// will appear. There are seven possible locations (primary to septenary), 
        /// each of which can be combined with the GH_Exposure.obscure flag, which 
        /// ensures the component will only be visible on panel dropdowns.
        /// </summary>
        public override GH_Exposure Exposure
        {
            get { return GH_Exposure.primary; }
        }

        /// <summary>
        /// Provides an Icon for every component that will be visible in the User Interface.
        /// Icons need to be 24x24 pixels.
        /// </summary>
        protected override System.Drawing.Bitmap Icon
        {
            get
            {
                // You can add image files to your project resources and access them like this:
                return Properties.Resources.ComponentIcon128;
            }
        }

        /// <summary>
        /// Each component must have a unique Guid to identify it. 
        /// It is vital this Guid doesn't change otherwise old ghx files 
        /// that use the old ID will partially fail during loading.
        /// </summary>
        public override Guid ComponentGuid
        {
            get { return new Guid("3754abf6-45f5-4c8c-aa61-96cea1ec48a3"); }
        }

        unsafe ~ShadowModelerGHComponent() {
            if (shadow_model_state == null) return;
            ShadowModelerUser.shmd_free_state(shadow_model_state);
            shadow_model_state = (IntPtr)null;
        }
    }
}
