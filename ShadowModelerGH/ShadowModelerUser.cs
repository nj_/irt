﻿using System;
using System.Runtime.InteropServices;

public static unsafe class ShadowModelerUser
{
    [DllImport("shadow_modeler.dll", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
    unsafe public static extern IntPtr shmd_init_state(IntPtr instance, int grid_resolution, int images_per_rotation);
    [DllImport("shadow_modeler.dll", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)] 
    unsafe public static extern void shmd_free_state(IntPtr instance);

    [DllImport("shadow_modeler.dll", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
    unsafe public static extern void shmd_add_projection(IntPtr instance, byte[] input_filename, float z_theta, float y_theta);

    [DllImport("shadow_modeler.dll", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
    unsafe public static extern void shmd_generate_volumetric_data(IntPtr instance);

    [DllImport("shadow_modeler.dll", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
    unsafe public static extern void shmd_maybe_allocate_vertex_buffer(IntPtr instance);


    [DllImport("shadow_modeler.dll", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
    unsafe public static extern int shmd_generate_pointcloud(IntPtr instance);

    [DllImport("shadow_modeler.dll", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
    unsafe public static extern int shmd_march_cubes(IntPtr instance);

    [DllImport("shadow_modeler.dll", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
    unsafe public static extern float* shmd_get_vertecies(IntPtr instance);

}
