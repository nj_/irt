﻿using Grasshopper.Kernel;
using System;
using System.Drawing;

namespace ShadowModelerGH {
    public class ShadowModelGHComponentInfo : GH_AssemblyInfo
    {
        public override string Name
        {
            get
            {
                return "ShadowModelerGH";
            }
        }
        public override Bitmap Icon
        {
            get
            {
                //Return a 24x24 pixel bitmap to represent this GHA library.
                return Properties.Resources.LibraryIcon24;
            }
        }
        public override string Description
        {
            get
            {
                //Return a short string describing the purpose of this GHA library.
                return "";
            }
        }
        public override Guid Id
        {
            get
            {
                return new Guid("69db683d-165e-40f4-b16d-31168af2a1f1");
            }
        }

        public override string AuthorName
        {
            get
            {
                //Return a string identifying you or your company.
                return "";
            }
        }
        public override string AuthorContact
        {
            get
            {
                //Return a string representing your preferred contact details.
                return "";
            }
        }
    }
}
