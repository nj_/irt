// Created by Noam Jacobi (2021)

#ifndef MARCHING_CUBES_H
#define MARCHING_CUBES_H

#include "handmade_math.h"

/* Cube vertex and edge indecies:
.                                               6                 
.      7----6                  7      6      7----6    
.     /|   /|                 /      /       |    | 5  
.    / |  / |       2      11/      /10    7 |    |    
.   3--4-2--5    3----2     3   4  2   5     4----5    
.   | /  | /     |    | 1      /      /        4       
.   |/   |/    3 |    |       /8     /9                 
.   0----1       0----1      0      1                   
.                  0                                   
*/


typedef struct Face {
    hmm_vec3 v0, v1, v2;
} Face;

typedef struct Cube {
    u64 face_count;
    Face faces[5];
} Cube;

#define VOXEL(b, x, y, z, r) (b[(x) + (y)*(r) + (z)*(r)*(r)])
#define VOXELP(b, x, y, z, r) (&b[(x) + (y)*(r) + (z)*(r)*(r)])

#if defined(__clang__)
#define PACK( __Declaration__ ) __attribute__((__packed__)) __Declaration__
#elif defined(_MSC_VER)
#define PACK( __Declaration__ ) __pragma( pack(push, 1) ) __Declaration__ __pragma( pack(pop))
#endif

typedef struct PACK(PlyFace) {
    u8 vertex_count;
    s32 indecies[3];
} PlyFace;

#ifdef __cplusplus 
extern "C" {
#endif
    
#undef DLL_DECL
#ifdef _WINDLL
#define DLL_DECL __declspec(dllexport)
#else
#define DLL_DECL __declspec(dllimport)
#endif
    
    DLL_DECL void export_binary_ply_model(char *export_filename, hmm_vec3 *vertex_buffer, int vertex_count, PlyFace *triangle_buffer, int triangle_count);
    DLL_DECL void export_ascii_ply_model(char *export_filename, hmm_vec3 *vertex_buffer, int vertex_count, PlyFace *triangle_buffer, int triangle_count);
    
    DLL_DECL void reconstruct_geometry_from_voxel_data(u8 *voxel_buffer, int cube_resolution, int cube_height, 
                                                       int images_per_rotation,
                                                       hmm_vec3 *vertex_buffer, int *vertex_count,
                                                       PlyFace *triangle_buffer, int *triangle_count);
#ifdef __cplusplus 
}
#endif
#endif //MARCHING_CUBES_H
