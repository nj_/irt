// Created by Noam Jacobi (2021)

#include "marching_cubes.h"
#include "marching_cube_array.h"
#include <stdio.h>

void march_cubes_on_voxels(u8 *voxel_buffer, int cube_resolution, int cube_height,
                           int images_per_rotation, hmm_vec3 *vertex_buffer, int *vertex_count) {
    
    int vertex_index = 0;
#define P(x0, y0, z0) (VOXEL(voxel_buffer, x+(x0), y+(y0), z+(z0), cube_resolution) > (images_per_rotation-1))
    for(int z = 0; z < cube_height; ++z) {
        for(int y = 0; y < cube_resolution - 1; ++y) {
            for(int x = 0; x < cube_resolution - 1; ++x) {
                
                /* Cube vertex and edge indecies:
                .                                               6                 
                .      7----6                  7      6      7----6    
                .     /|   /|                 /      /       |    | 5  
                .    / |  / |       2      11/      /10    7 |    |    
                .   3--4-2--5    3----2     3   4  2   5     4----5    
                .   | /  | /     |    | 1      /      /        4       
                .   |/   |/    3 |    |       /8     /9                 
                .   0----1       0----1      0      1                   
                .                  0                                   
                */
                
                u8 cube_index = ((P(0,0,0) << 0) |
                                 (P(1,0,0) << 1) |
                                 (P(1,0,1) << 2) |
                                 (P(0,0,1) << 3) |
                                 (P(0,1,1) << 7) |
                                 (P(0,1,0) << 4) |
                                 (P(1,1,0) << 5) |
                                 (P(1,1,1) << 6));
                
                Cube *cube = &marching_cube_array[cube_index];
                for(u64 f_i = 0; f_i < cube->face_count; ++f_i) {
                    
                    hmm_vec3 *v = &cube->faces[f_i].v0;
                    for(int i = 0; i < 3; ++i) {
                        float _x = ((2*(v[i].X + (float)x))/(float)cube_resolution) - 1.f;
                        float _y = ((2*(v[i].Y + (float)y))/(float)cube_resolution) - 1.f;
                        float _z = ((2*(v[i].Z + (float)z))/(float)cube_resolution) - 1.f;
                        vertex_buffer[vertex_index++] = HMM_Vec3(_x, _y, _z);
                    }
                }
            }
        }
    }
#undef P
    
    assert((vertex_index % 3) == 0);
    *vertex_count = vertex_index;
}

void reconstruct_geometry_from_voxel_data(u8 *voxel_buffer, int cube_resolution, int cube_height,
                                          int images_per_rotation, hmm_vec3 *vertex_buffer, int *vertex_count,
                                          PlyFace *triangle_buffer, int *triangle_count) {
    int z_output_increment = (cube_height/100) + 1;
    if(z_output_increment == 0) z_output_increment = cube_height - 1;
    
    int vertex_index = 0, triangle_index = 0;
    
#ifdef SHMD_DEBUG
    for(int z_start = 0; z_start < cube_height; z_start += z_output_increment) {
        int max_z = z_start + z_output_increment;
        if(max_z > cube_height - 1) max_z = cube_height - 1;
        
        printf("\rGenerating geometry: %3d%%", z_start/z_output_increment);
        fflush(stdout);
#else
        int z_start = 0;
        int max_z = cube_height;
#endif
        
#define P(x0, y0, z0) (VOXEL(voxel_buffer, x+(x0), y+(y0), z+(z0), cube_resolution) > (images_per_rotation-1))
        for(int z = z_start; z < max_z; ++z) {
            for(int y = 0; y < cube_resolution - 1; ++y) {
                for(int x = 0; x < cube_resolution - 1; ++x) {
                    // see comment in marching_cubes.h for how the cube indecies
                    // are ordered.
                    u8 cube_index = ((P(0,0,0) << 0) |
                                     (P(1,0,0) << 1) |
                                     (P(1,0,1) << 2) |
                                     (P(0,0,1) << 3) |
                                     (P(0,1,1) << 7) |
                                     (P(0,1,0) << 4) |
                                     (P(1,1,0) << 5) |
                                     (P(1,1,1) << 6));
                    Cube *cube = &marching_cube_array[cube_index];
                    for(u64 f_i = 0; f_i < cube->face_count; ++f_i) {
                        
                        PlyFace *face = &triangle_buffer[triangle_index++];
                        face->vertex_count = 3;
                        face->indecies[0] = vertex_index+0;
                        face->indecies[1] = vertex_index+1;
                        face->indecies[2] = vertex_index+2;
                        
                        hmm_vec3 *v = &cube->faces[f_i].v0;
                        for(int i = 0; i < 3; ++i) {
                            float _x = ((2*(v[i].X + (float)x))/(float)cube_resolution) - 1.f;
                            float _y = ((2*(v[i].Y + (float)y))/(float)cube_resolution) - 1.f;
                            float _z = ((2*(v[i].Z + (float)z))/(float)cube_resolution) - 1.f;
                            vertex_buffer[vertex_index++] = HMM_Vec3(_x, _y, _z);
                        }
                    }
                }
            }
        }
#undef P
        
#ifdef SHMD_DEBUG
    }
    
    printf("\rGenerating geometry: Done.\n");
#endif
    
    *vertex_count = vertex_index;
    *triangle_count = triangle_index;
}

void export_binary_ply_model(char *export_filename, hmm_vec3 *vertex_buffer, int vertex_count, PlyFace *triangle_buffer, int triangle_count) {
    FILE *ply_file;
    fopen_s(&ply_file, export_filename, "wb");
    fprintf(ply_file, "ply\n"
            "format binary_little_endian 1.0\n"
            "element vertex %d\n"
            "property float x\n"
            "property float y\n"
            "property float z\n"
            "element face %d\n"
            "property list uchar int vertex_index\n"
            "end_header\n",
            vertex_count, triangle_count);
    
    size_t count = fwrite(vertex_buffer, sizeof(hmm_vec3), vertex_count, ply_file);
    assert(count == (size_t)vertex_count);
    count = fwrite(triangle_buffer, sizeof(PlyFace), triangle_count, ply_file);
    assert(count == (size_t)triangle_count);
    
    fclose(ply_file);
}


void export_ascii_ply_model(char *export_filename, hmm_vec3 *vertex_buffer, int vertex_count, PlyFace *triangle_buffer, int triangle_count) {
    FILE *ply_file;
    fopen_s(&ply_file, export_filename, "w");
    fprintf(ply_file, "ply\n"
            "format ascii 1.0\n"
            "element vertex %d\n"
            "property float x\n"
            "property float y\n"
            "property float z\n"
            "element face %d\n"
            "property list uchar int vertex_index\n"
            "end_header\n",
            vertex_count, triangle_count);
    
    for(int i = 0; i < vertex_count; ++i) {
        hmm_vec3 v = vertex_buffer[i];
        fprintf(ply_file, "%f %f %f\n", v.X, v.Y, v.Z);
    }
    for(int i = 0; i < triangle_count; ++i) {
        PlyFace t = triangle_buffer[i];
        fprintf(ply_file, "%d %d %d %d\n", t.vertex_count, t.indecies[0], t.indecies[1], t.indecies[2]);
    }
    
    fclose(ply_file);
}
