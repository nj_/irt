// Created by Noam Jacobi (2021)

#include <windows.h>
#include <process.h>
#include <stdlib.h>
#include <stdint.h>
#include <math.h>

#if !SHMD_DEBUG
#define NDEBUG
#endif

#include <assert.h>

#include "shadow_modeler.h"

// #include "cube_generator.c"
#include "marching_cubes.c"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wall"
#endif

#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"

#define STB_IMAGE_WRITE_IMPLEMENTATION
#include "stb_image_write.h"

#define STB_IMAGE_RESIZE_IMPLEMENTATION
#include "stb_image_resize.h"

#define HANDMADE_MATH_IMPLEMENTATION
#include "handmade_math.h"

#ifdef __clang__
#pragma clang diagnostic pop
#endif

void calculate_projection_for_z_range(u8 *voxel_buffer, int grid_resolution, Projection *p,
                                      hmm_mat4 transform, int z_start, int z_end, u8 value) {
    for(int z = z_start; z < z_end; ++z) {
        for(int x = 1; x < grid_resolution-1; ++x) {
            for(int y = 1; y < grid_resolution-1; ++y) {
                // Coordinates of the model are inside a 2x2x2 cube centered at the origin,
                // so that the result of the rotation will still be centered at the origin.
                float _z = ((float)(2*(z-p->base_height))/(float)grid_resolution) - 1.f;
                float _y = ((float)(2*y)/(float)grid_resolution) - 1.f;
                float _x = ((float)(2*x)/(float)grid_resolution) - 1.f;
                hmm_vec4 voxel_coord = HMM_Vec4(_x, _y, _z, 1.f);
                hmm_vec4 rotated_coord = HMM_MultiplyMat4ByVec4(transform, voxel_coord);
                
                // Our image (after the rotation) is projected perpendicular to the x-axis.
                // As the projection is currently orthographic, getting our image
                // coordinates is simply done by dropping the x coordinate and normalizing
                // y and z to the width and height of the image.
                int u = (int)(((rotated_coord.Y + 1.f)/2.f)*(float)grid_resolution);
                int v = (int)(((rotated_coord.Z + 1.f)/2.f)*(float)grid_resolution);
                
                if(u < 0 || u >= grid_resolution) continue;
                if(v < 0 || v >= grid_resolution) continue;
                
                if(p->image_data[v*grid_resolution + u] > 127) {
                    VOXEL(voxel_buffer, x, y, z, grid_resolution) += value;
                }
            }
        }
    }
}

void projection_thread_work(void *param)
{
    ProjectionThreadData* data = (ProjectionThreadData *)param;
    ShadowModelState *state = data->state;
    
    const int spiral_step = (state->grid_resolution / state->images_per_rotation);
    
    for(int projection_index = data->index_start;
        projection_index < data->index_end;
        ++projection_index) {
        if(projection_index == 0) {
            for(int k = 0; k < state->images_per_rotation; ++k) {
                Projection *p = &state->projection_list[0];
                hmm_mat4 z_rotation_matrix = HMM_Rotate(-p->z_theta, HMM_Vec3(0, 0, 1));
                hmm_mat4 y_rotation_matrix = HMM_Rotate(-p->y_theta, HMM_Vec3(0, 1, 0));
                hmm_mat4 transform = HMM_MultiplyMat4(z_rotation_matrix, y_rotation_matrix);
                int z_start = p->base_height + k*spiral_step;
                int z_end   = z_start + spiral_step;
                if(z_end > state->grid_resolution) z_end = state->grid_resolution;
                u8 value = (u8)(state->images_per_rotation-k);
                calculate_projection_for_z_range(state->voxel_buffer, state->grid_resolution, p,
                                                 transform, z_start, z_end, value);
            }
        } else if(projection_index == state->auto_angle_count-1) {
            for(int k = 0; k < state->images_per_rotation; ++k) {
                Projection *p = &state->projection_list[state->auto_angle_count-1];
                hmm_mat4 z_rotation_matrix = HMM_Rotate(-p->z_theta, HMM_Vec3(0, 0, 1));
                hmm_mat4 y_rotation_matrix = HMM_Rotate(-p->y_theta, HMM_Vec3(0, 1, 0));
                hmm_mat4 transform = HMM_MultiplyMat4(z_rotation_matrix, y_rotation_matrix);
                int z_start = p->base_height + k*spiral_step;
                int z_end   = z_start + spiral_step;
                if(z_end > state->grid_height) z_end = state->grid_height;
                u8 value = (u8)(k+1);
                calculate_projection_for_z_range(state->voxel_buffer, state->grid_resolution, p,
                                                 transform, z_start, z_end, value);
            }
        } else {
            Projection *p = &state->projection_list[projection_index];
            hmm_mat4 z_rotation_matrix = HMM_Rotate(-p->z_theta, HMM_Vec3(0, 0, 1));
            hmm_mat4 y_rotation_matrix = HMM_Rotate(-p->y_theta, HMM_Vec3(0, 1, 0));
            hmm_mat4 transform = HMM_MultiplyMat4(z_rotation_matrix, y_rotation_matrix);
            
            int z_start = p->base_height;
            int z_end   = p->base_height + state->grid_resolution;
            if(projection_index + state->images_per_rotation < state->auto_angle_count) {
                Projection *next_level_p = &state->projection_list[projection_index + state->images_per_rotation];
                if(z_end > next_level_p->base_height) {
                    z_end = next_level_p->base_height;
                }
            }
            calculate_projection_for_z_range(state->voxel_buffer, state->grid_resolution, p,
                                             transform, z_start, z_end, 1);
        }
    }
    
    _endthread();
}


void delta_thread_work(void *param)
{
    DeltaThreadData* data = (DeltaThreadData *)param;
    ShadowModelState *state = data->state;
    const Projection *p = &state->projection_list[data->projection_index];
    const int res = state->grid_resolution;
    
    u8 *delta_buffer = malloc(sizeof(u8) * res * res * 3);
    memset(delta_buffer, 0, sizeof(u8) * res * res * 3);
    const char *output_path = data->output_path;
    
    hmm_mat4 z_rotation_matrix = HMM_Rotate(-p->z_theta, HMM_Vec3(0, 0, 1));
    hmm_mat4 y_rotation_matrix = HMM_Rotate(-p->y_theta, HMM_Vec3(0, 1, 0));
    hmm_mat4 rotation_matrix = HMM_MultiplyMat4(z_rotation_matrix, y_rotation_matrix);
    // hmm_mat4 perspective_matrix = HMM_Perspective(110.f, 1.f, 10.f, 0.f);
    hmm_mat4 transform = rotation_matrix; //HMM_MultiplyMat4(perspective_matrix, rotation_matrix);
    
    const char *image_filename = p->image_filename;
    const char *c = image_filename + strlen(image_filename)-1;
    size_t path_index = strlen(image_filename);
    while(*c && (*c != '/') && (*c != '\\') && (path_index > 0)) {
        --c;
        --path_index;
    }
    if(*c != 0) image_filename += path_index;
    char delta_filename[256];
    sprintf_s(delta_filename, 256, "%s/%s", output_path, image_filename);
    
    int z_start = p->base_height;
    int z_end   = z_start + res;
    
    for(int z = z_start; z < z_end; ++z) {
        for(int x = 0; x < res; ++x) {
            for(int y = 0; y < res; ++y) {
                float _z = ((float)(2*(z-p->base_height))/(float)res) - 1.f;
                float _y = ((float)(2*y)/(float)res) - 1.f;
                float _x = ((float)(2*x)/(float)res) - 1.f;
                hmm_vec4 voxel = HMM_Vec4(_x, _y, _z, 1.f);
                hmm_vec4 rv = HMM_MultiplyMat4ByVec4(transform, voxel);
                
                int u = (int)(((rv.Z + 1.f)/2.f)*(float)res);
                int v = (int)(((rv.Y + 1.f)/2.f)*(float)res);
                if(u < 0 || u >= res) continue;
                if(v < 0 || v >= res) continue;
                
                delta_buffer[(u*res + v)*3 + 1] |= (VOXEL(state->voxel_buffer, x, y, z, res) > state->voxel_gate)*0xFF;
                delta_buffer[(u*res + v)*3 + 0] |= (p->image_data[u*res + v] > 127)*0xFF;
                delta_buffer[(u*res + v)*3 + 2] |= (p->image_data[u*res + v] > 127)*0xFF;
            }
        }
    }
    
    stbi_write_png(delta_filename, res, res, 3, delta_buffer, 0);
    
    free(delta_buffer);
    
    _endthread();
}


SHMD_API_ADD_PROJECTION_SIG(shmd_add_projection) {
    ShadowModelState *state = (ShadowModelState *)_state;
    
    const int res = state->grid_resolution;
    const int spiral_step = (res / state->images_per_rotation);
    if(state->grid_height == 0) state->grid_height = res;
    
    Projection *p = &state->projection_list[state->projection_count];
    
    int width, height;
    u8 *data = stbi_load(image_filename, &width, &height, NULL, 1);
    if(data == NULL) {
        fprintf(stderr, "Could not find '%s'!\n", image_filename);
        return;
    }
    if(p->image_data != NULL) {
        stbi_image_free(p->image_data);
    }
    
    if(res == width && res == height) {
        p->image_data = data;
    } else {
        p->image_data = stbi__malloc(sizeof(u8) * res * res);
        stbir_resize_uint8(data, width, height, 0, p->image_data, res, res, 0, 1);
        stbi_image_free(data);
    }
    
    strcpy_s(p->image_filename, PROJECTION_FILENAME_SIZE, image_filename);
    if(z_theta == 360.f) {
        p->z_theta = (float)(state->auto_angle_count*180)/(float)(state->images_per_rotation);
        
        p->base_height = spiral_step*state->auto_angle_count;
        
        state->auto_angle_count++;
        state->grid_height += spiral_step;
    } else {
        p->z_theta = z_theta;
    }
    
    p->y_theta = y_theta;
    
    state->projection_count++;
}


SHMD_API_INIT_SIG(shmd_init_state) {
    ShadowModelState *state = (ShadowModelState *)_state;
    
    if((state != NULL) && ((state->grid_resolution != grid_resolution) || (state->images_per_rotation != images_per_rotation))) {
        shmd_free_state(_state);
        _state = (intptr_t)NULL;
        state = NULL;
    }
    
    if(state == NULL) {
        state = malloc(sizeof(ShadowModelState));
        
        if(state == NULL) return (intptr_t)NULL;
        
        memset(state, 0, sizeof(ShadowModelState));
        state->grid_resolution = grid_resolution;
        state->images_per_rotation = images_per_rotation;
        state->projection_list = malloc(MAX_PROJECTION_COUNT * sizeof(Projection));
        memset(state->projection_list, 0, MAX_PROJECTION_COUNT * sizeof(Projection));
        
        stbi_set_flip_vertically_on_load(1);
        stbi_flip_vertically_on_write(1);
        
        _state = (intptr_t)state;
    } 
    
    state->projection_count = 0;
    state->auto_angle_count = 0;
    
    return _state;
}

SHMD_API_SIG(shmd_free_state) {
    ShadowModelState *state = (ShadowModelState *)_state;
    if(state == NULL) return;
    
    for(int i = 0; i < state->projection_count; ++i) {
        Projection *p = &state->projection_list[i];
        if(p->image_data) {
            stbi_image_free(p->image_data);
            p->image_data = NULL;
        }
    }
    
    free(state->projection_list);
    
    if(state->voxel_buffer) {
        free(state->voxel_buffer);
        state->voxel_buffer = NULL;
    }
    
    if(state->vertex_buffer) {
        free(state->vertex_buffer);
        state->vertex_buffer = NULL;
    }
    
    free(state);
}

SHMD_API_SIG(shmd_generate_volumetric_data) {
    ShadowModelState *state = (ShadowModelState *)_state;
    if(state == NULL) return;
    
    if(state->grid_height < state->grid_resolution) state->grid_height = state->grid_resolution;
    
    const int res = state->grid_resolution;
    size_t voxel_buffer_size = res * res * state->grid_height;
    state->voxel_buffer = realloc(state->voxel_buffer, sizeof(u8) * voxel_buffer_size);
    memset(state->voxel_buffer, 0, sizeof(u8) * voxel_buffer_size);
    
    // printf("  %-24s (%5.1f, %5.1f): ", p->image_filename, p->z_theta, p->y_theta);
    ProjectionThreadData base_data = {0};
    base_data.state = state;
    
#define MAX_THREAD_COUNT 64
    ProjectionThreadData thread_datas[MAX_THREAD_COUNT] = {0};
    HANDLE thread_handles[MAX_THREAD_COUNT] = {0};
    int thread_count = 0;
    
    for(int index_start = 0; index_start < (state->auto_angle_count-1); 
        index_start += state->images_per_rotation) {
        int index_end = index_start + state->images_per_rotation;
        if(index_end > state->auto_angle_count) index_end = state->auto_angle_count;
        
        ProjectionThreadData *data = &thread_datas[thread_count];
        *data = base_data;
        data->index_start = index_start;
        data->index_end   = index_end;
        
        thread_handles[thread_count] = (HANDLE)_beginthread(projection_thread_work, 0, data);
        
        ++thread_count;
    }
    WaitForMultipleObjectsEx(thread_count, thread_handles, 1, INFINITE, 0);
    
    memset(VOXELP(state->voxel_buffer, 0, 0, 0, state->grid_resolution), 0, state->grid_resolution * state->grid_resolution);
    memset(VOXELP(state->voxel_buffer, 0, 0, state->grid_height-1, state->grid_resolution), 0, state->grid_resolution * state->grid_resolution);
    
    shmd_update_voxel_info((intptr_t)state);
}

SHMD_API_SIG(shmd_update_voxel_info) {
    ShadowModelState *state = (ShadowModelState *)_state;
    if(state == NULL) return;
    
    state->voxel_gate = (u8)(state->images_per_rotation-1);
    
    int voxel_count = 0;
    const int res = state->grid_resolution;
    
    for(int z = 0; z < state->grid_height; ++z) {
        for(int y = 0; y < res; ++y) {
            for(int x = 0; x < res; ++x) {
                voxel_count += (VOXEL(state->voxel_buffer, x, y, z, res) > state->voxel_gate);
            }
        }
    }
    
    state->voxel_count = voxel_count;
}

SHMD_API_SIG(shmd_maybe_allocate_vertex_buffer) {
    ShadowModelState *state = (ShadowModelState *)_state;
    if(state == NULL) return;
    
    if((state->vertex_buffer == NULL) || (state->cached_voxel_count != state->voxel_count)) {
        state->vertex_buffer = realloc(state->vertex_buffer, state->voxel_count * 15 * sizeof(hmm_vec3));
        state->cached_voxel_count = state->voxel_count;
    }
}

SHMD_API_SIG(shmd_export_image_deltas) {
    ShadowModelState *state = (ShadowModelState *)_state;
    if(state == NULL) return;
    
    DeltaThreadData *thread_datas = malloc(sizeof(DeltaThreadData)*state->projection_count);
    HANDLE *thread_handles = malloc(sizeof(HANDLE)*state->projection_count);
    
    DeltaThreadData base_data = {0};
    base_data.state = state;
    base_data.output_path = "delta";
    
    for(int i = 0; i < state->projection_count; ++i) {
        DeltaThreadData *data = &thread_datas[i];
        *data = base_data;
        data->projection_index = i;
        thread_handles[i] = (HANDLE)_beginthread(delta_thread_work, 0, data);
    }
    
    WaitForMultipleObjectsEx(state->projection_count, thread_handles, 1, INFINITE, 0);
    
    free(thread_datas);
    free(thread_handles);
}

SHMD_API_GENERATE_GEOMETRY_SIG(shmd_generate_pointcloud) {
    ShadowModelState *state = (ShadowModelState *)_state;
    if(state == NULL) return 0;
    
    int z_output_increment = (state->grid_height/100) + 1;
    if(z_output_increment == 0) z_output_increment = state->grid_height - 1;
    
    state->vertex_count = 0;
    const int res = state->grid_resolution;
    
    for(int z_start = 0; z_start < state->grid_height; z_start += z_output_increment) {
        int max_z = z_start + z_output_increment;
        if(max_z >= state->grid_height - 1) max_z = state->grid_height - 1;
        
#if SHMD_DEBUG
        printf("\rGenerating pointcloud: %3d%%", z_start/z_output_increment);
        fflush(stdout);
#endif
        
        for(int z = z_start; z < max_z; ++z) {
            for(int y = 0; y < res; ++y) {
                for(int x = 0; x < res; ++x) {
                    u8 value = VOXEL(state->voxel_buffer, x, y, z, res);
                    if(value > state->voxel_gate) {
                        float _x = ((2*(float)(x))/(float)res) - 1.f;
                        float _y = ((2*(float)(y))/(float)res) - 1.f;
                        float _z = ((2*(float)(z))/(float)res) - 1.f;
                        state->vertex_buffer[state->vertex_count++] = HMM_Vec3(_x, _y, _z);
                    }
                }
            }
        }
    }
    assert(state->voxel_count >= state->vertex_count);
    
#if SHMD_DEBUG
    printf("\rGenerating pointcloud: Done.\n");
#endif
    
    return state->vertex_count;
}

SHMD_API_SIG(shmd_generate_sinogram_images) {
    ShadowModelState *state = (ShadowModelState *)_state;
    if(state == NULL) return;
    
    int z_output_increment = (state->grid_height/100) + 1;
    if(z_output_increment == 0) z_output_increment = state->grid_height - 1;
    
    const int res = state->grid_resolution;
    
    u8 *slice_buffer = malloc(sizeof(u8) * res * res * 3);
    
    for(int z_start = 0; z_start < state->grid_height; z_start += z_output_increment) {
        int max_z = z_start + z_output_increment;
        if(max_z >= state->grid_height - 1) max_z = state->grid_height - 1;
        
#if SHMD_DEBUG
        printf("\rGenerating sinograms: %3d%%", z_start/z_output_increment);
        fflush(stdout);
#endif
        
        for(int z = z_start; z < max_z; ++z) {
            memset(slice_buffer, 0, sizeof(u8) * res * res);
            
            for(int y = 0; y < res; ++y) {
                for(int x = 0; x < res; ++x) {
                    u8 value = VOXEL(state->voxel_buffer, x, y, z, res);
                    slice_buffer[y*res + x] = (u8)((float)value/(float)state->images_per_rotation*255.f);
                }
            }
            
            static int index = 0;
            char export_filename[256];
            sprintf_s(export_filename, 256, "z-slices/slice_%04d.png", ++index);
            stbi_write_png(export_filename, res, res, 1, slice_buffer, 0);
        }
    }
    
#if SHMD_DEBUG
    printf("\rGenerating pointcloud: Done.\n");
#endif
    
    free(slice_buffer);
}

SHMD_API_GENERATE_GEOMETRY_SIG(shmd_march_cubes) {
    ShadowModelState *state = (ShadowModelState *)_state;
    if(state == NULL) return 0;
    
    shmd_maybe_allocate_vertex_buffer((intptr_t)state);
    march_cubes_on_voxels(state->voxel_buffer, state->grid_resolution, state->grid_height, state->images_per_rotation,
                          state->vertex_buffer, &state->vertex_count);
    
    int triangle_count = state->vertex_count/3;
    return triangle_count;
}


SHMD_API_GET_TRIANGLES_SIG(shmd_get_vertecies) {
    ShadowModelState *state = (ShadowModelState *)_state;
    
    if(state == NULL) return NULL;
    float *buffer = (float *)state->vertex_buffer;
    
    return buffer;
}