// Created by Noam Jacobi (2021)

#ifndef SHADOW_MODELER_H
#define SHADOW_MODELER_H
#include "handmade_math.h"

typedef int8_t  s8;
typedef int16_t s16;
typedef int32_t s32;
typedef int64_t s64;

typedef uint8_t  u8;
typedef uint16_t u16;
typedef uint32_t u32;
typedef uint64_t u64;

typedef s8 bool;
#define true  1
#define false 0

#define F_PI 3.14159265358979323846f
#define DEG_TO_RAD(angle) ((F_PI/180.f)*(angle))
#define RAD_TO_DEG(angle) ((180.f/F_PI)*(angle))

#define PROJECTION_FILENAME_SIZE 256
typedef struct Projection {
    u8 *image_data;
    char image_filename[PROJECTION_FILENAME_SIZE];
    float z_theta;
    float y_theta;
    
    int base_height;
} Projection;

#define MAX_PROJECTION_COUNT 128
typedef struct ShadowModelState {
    int grid_resolution;
    int grid_height;
    int images_per_rotation;
    int projection_count;
    int auto_angle_count;
    Projection *projection_list;
    
    u8 *voxel_buffer;
    int voxel_count;
    int cached_voxel_count;
    u8 voxel_gate;
    
    hmm_vec3 *vertex_buffer;
    int vertex_count;
    
    int spiral_step; // cached, depends on grid_resolution, images_per_rotation
} ShadowModelState;

// ------ Main API
#ifdef __cplusplus 
extern "C" {
#endif
    
#undef SM_API_SIG
#undef SM_API_PARSE_INPUT_STRING_SIG
#ifdef _WINDLL
#define DLL_DECL __declspec(dllexport)
#else
#pragma comment(lib, "shadow_modeler.lib")
#define DLL_DECL __declspec(dllimport)
#endif
    
#define SHMD_API_INIT_SIG(name)               DLL_DECL extern intptr_t (name)(intptr_t _state, int grid_resolution, int images_per_rotation)
#define SHMD_API_SIG(name)                    DLL_DECL extern void (name)(intptr_t _state)
#define SHMD_API_ADD_PROJECTION_SIG(name)     DLL_DECL extern void (name)(intptr_t _state, char *image_filename, float z_theta, float y_theta)
#define SHMD_API_LOAD_PROJECTIONS_SIG(name)   DLL_DECL extern void (name)(intptr_t _state, char *image_library_path)
#define SHMD_API_GENERATE_GEOMETRY_SIG(name)  DLL_DECL extern int  (name)(intptr_t _state)
#define SHMD_API_GET_TRIANGLES_SIG(name)      DLL_DECL extern float *(name)(intptr_t _state)
    
    SHMD_API_GET_TRIANGLES_SIG(shmd_get_vertecies);
    
    SHMD_API_ADD_PROJECTION_SIG(shmd_add_projection);
    
    SHMD_API_INIT_SIG(shmd_init_state);
    
    SHMD_API_SIG(shmd_free_state);
    SHMD_API_SIG(shmd_update_voxel_info);
    SHMD_API_SIG(shmd_maybe_allocate_vertex_buffer);
    
    SHMD_API_SIG(shmd_export_image_deltas);
    SHMD_API_SIG(shmd_generate_volumetric_data);
    SHMD_API_SIG(shmd_generate_sinogram_images);
    
    SHMD_API_GENERATE_GEOMETRY_SIG(shmd_generate_pointcloud);
    SHMD_API_GENERATE_GEOMETRY_SIG(shmd_march_cubes);
    
#ifdef __cplusplus 
}
#endif

// ------ Internal thread stuff
typedef struct ProjectionThreadData {
    ShadowModelState *state;
    int index_start;
    int index_end;
} ProjectionThreadData;

void projection_thread_work(void *param);

typedef struct DeltaThreadData {
    ShadowModelState *state;
    int projection_index;
    
    char *output_path;
} DeltaThreadData;

void delta_thread_work(void *param);

#endif //SHADOW_MODELER_H
