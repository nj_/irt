// Created by Noam Jacobi (2021)

#include <windows.h> 
#include <stdlib.h>
#include <stdio.h>

#include "shadow_modeler.h"
#include "marching_cubes.h"
#define MAX_LINE_LENGTH 256

void parse_input_string(ShadowModelState *state, char *input_string, bool absolute_path) {
    if(input_string == NULL || state == NULL) return;
    state->projection_count = 0;
    
    char *next_line_token;
    char *line_token = strtok_s(input_string, "\n\r", &next_line_token);
    char input_dir_name[PROJECTION_FILENAME_SIZE];
    
    int line_index = -1;
    int parsed_index = 0;
    while(true) {
        ++line_index;
        if(line_index > 0) {
            line_token = strtok_s(NULL, "\n\r", &next_line_token);
        }
        if(line_token == NULL) break;
        
        char *line_start = line_token;
        while(((*line_start == '\t') || (*line_start == ' ')) && *line_start != '\0') ++line_start;
        if(*line_start == '\0') continue;
        if(*line_start == '#')  continue;
        if(*line_start == '\n') continue;
        if(*line_start == '\r') continue;
        
        if(strlen(line_start) > MAX_LINE_LENGTH) {
            fprintf(stderr, "Line %d is too long, ignoring it!\n", line_index);
            continue;
        }
        char line[MAX_LINE_LENGTH];
        strcpy_s(line, MAX_LINE_LENGTH, line_start);
        
        const char *sep = absolute_path ? ";" : " ";
        
        if(parsed_index == 0) {
            char *next_token = NULL;
            char *token = strtok_s(line, sep, &next_token);
            
            if(token != NULL) {
                strcpy_s(input_dir_name, PROJECTION_FILENAME_SIZE, token);
                token = strtok_s(NULL, sep, &next_token);
            } else {
                continue;
            }
            
            if(token != NULL) {
                state->grid_resolution = atoi(token);
                token = strtok_s(NULL, sep, &next_token);
            }
            
            if(token != NULL) {
                state->images_per_rotation = atoi(token);
            }
            
            printf("Input directory: %s, Cube resolution: %d, Images per rotation: %d\n", input_dir_name, state->grid_resolution, state->images_per_rotation);
        } else {
            char image_filename[PROJECTION_FILENAME_SIZE];
            strncpy_s(image_filename, PROJECTION_FILENAME_SIZE, input_dir_name, strlen(input_dir_name));
            size_t input_str_len = PROJECTION_FILENAME_SIZE;
            char *input_str = image_filename;
            if(!absolute_path) {
                input_str_len -= strlen(input_dir_name);
                input_str += strlen(input_dir_name);
                *input_str++ = '/';
            }
            
            char *next_token = NULL;
            char *token = strtok_s(line, sep, &next_token);
            
            if(token != NULL) {
                strcpy_s(input_str, input_str_len, token);
                token = strtok_s(NULL, sep, &next_token);
            } else {
                continue;
            }
            
            float z_theta, y_theta;
            if(token != NULL) {
                z_theta = (float)atof(token);
                token = strtok_s(NULL, sep, &next_token);
            } else {
                z_theta = (float)360.f;
            }
            
            if(token != NULL) {
                y_theta = (float)atof(token);
            } else {
                y_theta = (float)0.f;
            }
            
            shmd_add_projection((intptr_t)state, image_filename, z_theta, y_theta);
        }
        
        parsed_index++;
    }
}

void parse_input_file(ShadowModelState *state, const char *input_filename) {
    FILE *input_file;
    fopen_s(&input_file, input_filename, "r");
    if(!input_file) {
        fprintf(stderr, "Could not find input file '%s'!\n", input_filename);
        exit(EXIT_FAILURE);
    }
    
    fseek(input_file, 0, SEEK_END);
    int file_size = ftell(input_file);
    fseek(input_file, 0, SEEK_SET);
    
    char *input_string = malloc(file_size + 1);
    fread(input_string, 1, file_size, input_file);
    fclose(input_file);
    
    input_string[file_size] = '\0';
    
    parse_input_string(state, input_string, false);
    free(input_string);
}

int main(int argc, char *argv[]) {
    if(argc < 2) {
        fprintf(stdout, "Usage: %s <input directory> <cube resolution> <images per rotation>\n", argv[0]);
        return EXIT_SUCCESS;
    }
    
    if(strlen(argv[1]) >= MAX_PATH - 6) {
        fprintf(stderr, "Invalid input directory %s\n", argv[1]);
        return EXIT_FAILURE;
    }
    
    int cube_resolution = 64;
    int images_per_rotation = 3;
    if(argc >= 3) {
        int number = atoi(argv[2]);
        if(number <= 0) {
            fprintf(stderr,
                    "Cube resolution must be positive (a power of 2 is recommended)!\n"
                    "Using the default value: %d\n", cube_resolution);
        } else {
            if(__popcnt(number) != 1) {
                fprintf(stdout,
                        "A power of 2 is recommended for the cube resolution! (given %d)\n",
                        cube_resolution);
            }
            cube_resolution = number;
        }
    }
    if(argc >= 4) {
        int number = atoi(argv[3]);
        if(number <= 0) {
            fprintf(stderr,
                    "Imeges per rotation must be positive!\n"
                    "Using the value: %d\n", images_per_rotation);
        } else {
            images_per_rotation = number;
        }
    }
    
    ShadowModelState *state = (ShadowModelState *)shmd_init_state((intptr_t)NULL, cube_resolution, images_per_rotation);
    
    char input_path[MAX_PATH];
    sprintf_s(input_path, MAX_PATH, "%s/*.png", argv[1]);
    
    WIN32_FIND_DATA file_find_data;
    HANDLE file_find_handle = FindFirstFile(input_path, &file_find_data);
    
    if(file_find_handle == INVALID_HANDLE_VALUE) {
        fprintf(stderr, "Could not read projections from '%s'\n", argv[1]);
        return EXIT_FAILURE;
    }
    
    do {
        if (file_find_data.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) continue;
        sprintf_s(input_path, MAX_PATH, "%s/%s", argv[1], file_find_data.cFileName);
        
        shmd_add_projection((intptr_t)state, input_path, 360.f, 360.f);
    } while(FindNextFile(file_find_handle, &file_find_data) != 0);
    
    printf("Generating volumetric data: ");
    shmd_generate_volumetric_data((intptr_t)state);
    printf(" Done.\n");
    
    shmd_maybe_allocate_vertex_buffer((intptr_t)state);
    printf("Calculated %d voxels from %d projections.\n", state->voxel_count, state->projection_count);
    
    printf("Calculating Deltas: ");
    shmd_export_image_deltas((intptr_t)state);
    printf("Done.\n");
    
    shmd_generate_pointcloud((intptr_t)state);
    
    char *ply_pointcloud_filename = "generated_pointcloud.ply";
    printf("Saving '%s': ", ply_pointcloud_filename);
    export_binary_ply_model(ply_pointcloud_filename, state->vertex_buffer, state->voxel_count, NULL, 0);
    printf("Done.\n");
    
    size_t triangle_buffer_size = state->grid_resolution * state->grid_resolution * state->grid_height;
    
    PlyFace *triangle_buffer = malloc(sizeof(PlyFace) * triangle_buffer_size);
    
    int triangle_count = 0;
    reconstruct_geometry_from_voxel_data(state->voxel_buffer, state->grid_resolution, state->grid_height, state->images_per_rotation,
                                         state->vertex_buffer, &state->vertex_count, triangle_buffer, &triangle_count);
    
    char *ply_export_filename = "generated_model.ply";
    printf("Saving '%s': ", ply_export_filename);
    export_binary_ply_model(ply_export_filename, state->vertex_buffer, state->vertex_count,
                            triangle_buffer, triangle_count);
    printf("Done.\n");
    
    printf("Generated %d vertecies and %d triangles.\n", state->vertex_count, triangle_count);
    
    
    printf("Press ENTER to close the window\n");
    shmd_free_state((intptr_t)state);
    state = NULL;
    
    getchar();
    
    return 0;
}